package com.example.quiz.controller;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.quiz.R;
import com.example.quiz.model.QuizQuestion;
import com.example.quiz.view.FilmViewHolder;

import java.util.List;

public class CardStackAdapter extends RecyclerView.Adapter<FilmViewHolder> {

    private Context context;
    private List<QuizQuestion> filmQuestions;
    private LayoutInflater layoutInflater;

    public  CardStackAdapter(Context context, List<QuizQuestion>filmQuestions){
        this.context = context;
        this.filmQuestions = filmQuestions;
        layoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public FilmViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.film_view, parent, false);
        return new FilmViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FilmViewHolder holder, int position) {
        holder.getFilmQuestionText().setText(filmQuestions.get(position).getQuestionText());
        holder.getCategoryText().setText(filmQuestions.get(position).getCategoryText());
        holder.getDifficultyLevelText().setText(filmQuestions.get(position).getDifficultyLevelText());
        holder.getTrueButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(filmQuestions.get(position).isTrueAnswer()){
                    Toast.makeText(context,"True is tapped",Toast.LENGTH_SHORT).show();
                }else{

                }
            }
        });

        holder.getFalseButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context,"False is tapped",Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return filmQuestions.size();
    }
}
