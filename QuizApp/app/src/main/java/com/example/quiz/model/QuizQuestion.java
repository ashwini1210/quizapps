package com.example.quiz.model;

public class QuizQuestion {

    private String questionText;
    private String categoryText;
    private String difficultyLevelText;
    private boolean trueAnswer;

    public QuizQuestion(String questionText, String categoryText, String difficultyLevelText, boolean trueAnswer) {
        this.questionText = questionText;
        this.trueAnswer = trueAnswer;
        this.categoryText = categoryText;
        this.difficultyLevelText = difficultyLevelText;
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public boolean isTrueAnswer() {
        return trueAnswer;
    }

    public void setTrueAnswer(boolean trueAnswer) {
        this.trueAnswer = trueAnswer;
    }

    public String getCategoryText() {
        return categoryText;
    }

    public void setCategoryText(String categoryText) {
        this.categoryText = categoryText;
    }

    public String getDifficultyLevelText() {
        return difficultyLevelText;
    }

    public void setDifficultyLevelText(String difficultyLevelText) {
        this.difficultyLevelText = difficultyLevelText;
    }
}

