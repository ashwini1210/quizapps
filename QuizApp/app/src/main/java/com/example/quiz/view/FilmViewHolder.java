package com.example.quiz.view;

import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.quiz.R;

public class FilmViewHolder extends RecyclerView.ViewHolder {

    private TextView filmQuestionText;
    private TextView categoryText;
    private TextView difficultyLevelText;
    private ImageButton trueButton;
    private  ImageButton falseButton;

    public FilmViewHolder(@NonNull View itemView) {
        super(itemView);
        filmQuestionText = itemView.findViewById(R.id.film_question_text);
        categoryText = itemView.findViewById(R.id.category);
        difficultyLevelText = itemView.findViewById(R.id.difficulty_level);
        trueButton = itemView.findViewById(R.id.trueButton);
        falseButton = itemView.findViewById(R.id.falseButton);
    }

    public TextView getFilmQuestionText() {
        return filmQuestionText;
    }

    public TextView getCategoryText() {
        return categoryText;
    }

    public TextView getDifficultyLevelText() {
        return difficultyLevelText;
    }

    public ImageButton getTrueButton() {
        return trueButton;
    }

    public ImageButton getFalseButton() {
        return falseButton;
    }


}
