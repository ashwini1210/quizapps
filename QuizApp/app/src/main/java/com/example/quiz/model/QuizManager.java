package com.example.quiz.model;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.quiz.R;
import com.example.quiz.VolleySingleton;
import com.example.quiz.controller.CardStackAdapter;
import com.yuyakaido.android.cardstackview.CardStackLayoutManager;
import com.yuyakaido.android.cardstackview.CardStackView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class QuizManager {

    private Context context;
    private RequestQueue requestQueue;
    private String url;


    public QuizManager (Context context){
        this.context = context;
        requestQueue = VolleySingleton.getInstance().getRequestQueue();
        url = "https://opentdb.com/api.php?amount=10&category=11&type=boolean";
    }


    /**
     * Request on server to get response of Quiz question
     */
    public List<QuizQuestion> getQuizQuestion(Activity activity){

        List<QuizQuestion> quizQuestions = new ArrayList<>();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONArray result =  response.getJSONArray("results");
                    for (int i = 0; i < result.length(); i++ ){
                        JSONObject questionJsonQbj = result.getJSONObject(i);
                        String questionText = questionJsonQbj.getString("question");
                        String categoryText = questionJsonQbj.getString("category");
                        String difficultyLevelText = questionJsonQbj.getString("difficulty");
                        difficultyLevelText = difficultyLevelText.substring(0, 1).toUpperCase() + difficultyLevelText.substring(1).toLowerCase();
                        boolean questionAns = questionJsonQbj.getBoolean("correct_answer");
                        QuizQuestion quizQuestion = new QuizQuestion(questionText,categoryText,difficultyLevelText,questionAns);
                        quizQuestions.add(quizQuestion);
                    }
                    initCardStackView(activity,quizQuestions);
                } catch (JSONException exception) {
                    exception.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error", error.getMessage());
            }
        });
        requestQueue.add(jsonObjectRequest);
        return quizQuestions;
    }

    /**
     * Initialize CardStackView
     */
    private void initCardStackView(Activity activity, List<QuizQuestion> quizQuestions){
        CardStackView cardStackView = activity.findViewById(R.id.myCardStackView);
        cardStackView.setLayoutManager(new CardStackLayoutManager(context));
        cardStackView.setAdapter(new CardStackAdapter(context,quizQuestions));
    }
}
