# Quiz

App to play quiz about the Film world.

## Summery

Version of Quiz App:
1. Android 

## QuizApp (Android):

This app is developed to display data from server using Volley library.
Also implemented swipe feature like Tinder to show next quiz question.

### Installation

Clone this repository and import into Android Studio

`
https://ashwini1210@bitbucket.org/ashwini1210/quizapps.git
`

Use branch `Quiz_Android` to access android app.

1. Open QuizApp in the Android studio.
2. Run an App on the simulator or device

### Dependencies

com.android.volley:volley:1.1.1

com.yuyakaido.android:card-stack-view:${LatestVersion}


### API:
`https://opentdb.com/api.php?amount=10&category=11&type=boolean`






